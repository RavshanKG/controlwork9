import React from 'react';
import axios from 'axios';
import {Button, ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import './AddContact.css';

class AddContact extends React.Component {
  state = {
      name: '',
      number: '',
      email: '',
      url: ''
  }

  HandleChangeName = e =>
    this.setState({name: e.target.value});

  HandleChangeNumber = e =>
    this.setState({number: e.target.value});

  HandleChangeEmail = e =>
    this.setState({email: e.target.value});

  HandleChangeUrl = e =>
    this.setState({url: e.target.value});

  addContact = event => {
    event.preventDefault();

    let contact = {
      name: this.state.name,
      number: this.state.number,
      email: this.state.email,
      url: this.state.url,
    };

    axios.post('https://controlwork9.firebaseio.com/contacts.json', contact).then(() => {
      this.props.history.push('/');
    });
  };


  render () {
    console.log(this.state)
    return (
      <div className="container">
        <form >
          <h3 className="">Add new contact</h3>
          <FormGroup controlId="formControlsSelectMultiple">
            <ControlLabel>Name</ControlLabel>
            <FormControl name="name" type="text"
                         value={this.state.name}
                         onChange={this.HandleChangeName}>
            </FormControl>
          </FormGroup>


          <FormGroup controlId="formControlsSelectMultiple">
            <ControlLabel>Phone</ControlLabel>
            <FormControl name="number" type="text"
                        value={this.state.number}
                        onChange={this.HandleChangeNumber}>
            </FormControl>
          </FormGroup>


          <FormGroup controlId="formControlsSelectMultiple">
            <ControlLabel>E-mail</ControlLabel>
            <FormControl name="email" type="text"
                         value={this.state.email}
                         onChange={this.HandleChangeEmail}>
            </FormControl>
          </FormGroup>


          <FormGroup controlId="formControlsSelectMultiple">
            <ControlLabel>Photo</ControlLabel>
            <FormControl name="url" type="text"
                         value={this.state.url}
                         onChange={this.HandleChangeUrl}>
            </FormControl>
          </FormGroup>


          <img src={this.state.url} alt="" height='100px' weight='100px'/>

          <Button type={"submit"} className="submit"
                  onClick={this.addContact}>Submit</Button>
          <Button className="back">Back to contacts</Button>
        </form>
      </div>
    )
  }
}

export default AddContact;