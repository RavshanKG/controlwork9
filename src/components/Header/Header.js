import React from 'react';
import { Nav, Navbar} from "react-bootstrap";
import './Header.css';

import NavLink from "react-router-dom/es/NavLink";

const Header = () => {
  return (
    <Navbar inverse collapseOnSelect>
      <Navbar.Header>
        <Navbar.Brand>
          <NavLink to="/" exact> Contacts</NavLink>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav pullRight className="buttons">
          <NavLink to="/addContact" exact className="button">
            Add new contact
          </NavLink>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}


export default Header;

