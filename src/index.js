import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker'
import axios from 'axios';
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";

/*
axios.defaults.baseURL = 'https://controlwork9.firebaseio.com/';
*/



const Application = (
  <Provider >
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
)

ReactDOM.render(Application, document.getElementById('root'));
registerServiceWorker();
