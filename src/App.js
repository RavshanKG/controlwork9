import React, { Component } from 'react';
import './App.css';
import Header from "./components/Header/Header";
import {Route, Switch} from "react-router";
import AddContact from "./components/AddContact/AddContact";
import Contacts from "./containers/Contacts/Contacts";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Switch>
          <Route path="/" exact component={Contacts}/>
          <Route path="/addContact" exact component={AddContact}/>
        </Switch>
      </div>
    );
  }
}

export default App;
