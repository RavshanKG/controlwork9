import React from 'react';
import axios from 'axios';
import './Contacts.css';

class Contacts extends React.Component {
  state = {
    contactsList: [],
  }

  componentDidMount() {
    axios.get('https://controlwork9.firebaseio.com/contacts.json').then(response => {
      console.log(response.data);
      this.setState({contactsList: response.data})
    })
  }

  render(){
    return (
      Object.keys(this.state.contactsList).map(id => (
        <div className="container" key={id} >
          <img src={this.state.contactsList[id].url} alt="" width = '60px' height='60px' className="img"/>
          <h4>{this.state.contactsList[id].name}</h4>
        </div>
      ))
    )
  };
}

export default Contacts;